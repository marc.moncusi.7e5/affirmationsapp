package com.example.affirmations

import android.content.Intent
import androidx.annotation.StringRes
import androidx.compose.animation.animateContentSize
import androidx.compose.animation.core.Spring
import androidx.compose.animation.core.spring
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ExpandLess
import androidx.compose.material.icons.filled.ExpandMore
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.affirmations.model.Affirmation
import com.example.affirmations.ui.theme.AffirmationsTheme
import com.example.affirmations.viewmodel.MainViewModel

@Composable
fun AffirmationApp(mainViewModel: MainViewModel = MainViewModel()) {
  val uiState by mainViewModel.uiState.collectAsState()
  AffirmationsTheme {
    AffirmationList(uiState)

  }
}

@Composable
fun AffirmationList(affirmationList: List<Affirmation>) {
  LazyColumn {
    items(affirmationList) { affirmation -> AffirmationCard(affirmation) }
  }
}

@Composable
fun AffirmationCard(affirmation: Affirmation, modifier: Modifier = Modifier) {
  var expanded by remember { mutableStateOf(false) }

  val imageModifier = Modifier
    .fillMaxWidth()
    .height(194.dp)
    .border(BorderStroke(3.dp, MaterialTheme.colors.secondary))

  val textModifier = Modifier
    .padding(8.dp)
    .width(290.dp)

  Card(modifier = modifier.padding(8.dp), elevation = 1.dp) {
    Column(
      modifier = Modifier
        .animateContentSize(
          animationSpec = spring(
            dampingRatio = Spring.DampingRatioMediumBouncy,
            stiffness = Spring.StiffnessLow
          )
        )
    ) {
      Box(
        modifier = Modifier
          .fillMaxWidth(),
        contentAlignment = Alignment.BottomStart
      ) {
        Image(
          painter = painterResource(id = affirmation.imageResourceId),
          contentDescription = "Stock Image",
          contentScale = ContentScale.Crop,
          modifier = imageModifier
        )
        Row(
          modifier = Modifier
            .fillMaxWidth()
        ) {
          Text(
            text = stringResource(id = affirmation.stringResourceId),
            style = MaterialTheme.typography.h6,
            modifier = textModifier
          )
          Spacer(Modifier.weight(1f))
          Button(
            expanded = expanded,
            onClick = { expanded = !expanded },
          )
        }
      }
      if (expanded) {
        CardInformation(affirmation.descriptionResourceId, affirmation.id)
      }
    }

  }
}

@Composable
private fun Button(
  expanded: Boolean,
  onClick: () -> Unit,
) {
  IconButton(onClick = onClick) {
    Icon(
      imageVector = if (expanded) Icons.Filled.ExpandLess else Icons.Filled.ExpandMore,
      tint = MaterialTheme.colors.primary,
      contentDescription = stringResource(
        id = R.string.description
      )
    )
  }
}

@Composable
fun CardInformation(@StringRes description: Int, Id: Int, modifier: Modifier = Modifier) {
  val mContext = LocalContext.current
  Column(
    modifier = modifier.padding(
      start = 16.dp,
      top = 8.dp,
      bottom = 16.dp,
      end = 16.dp
    )
  ) {
    Text(
      text = stringResource(R.string.desc_name),
      style = MaterialTheme.typography.h6
    )
    Text(
      text = stringResource(description),
      style = MaterialTheme.typography.body1,
      overflow = TextOverflow.Ellipsis,
      maxLines =2
    )
    TextButton(
      onClick = {
        mContext.startActivity(
          Intent(
            mContext,
            DetailActivity::class.java
          ).putExtra("affirmationId", Id)
        )
      },
    ){
      Text(text = "See more")
    }
  }
}

@Preview
@Composable
private fun AffirmationCardPreview() {
  AffirmationCard(
    Affirmation(
      1,
      R.string.affirmation10,
      R.drawable.image10,
      R.string.description,
      R.string.emailTo,
      R.string.emailSubject,
      R.string.phoneNumber
    )
  )
}
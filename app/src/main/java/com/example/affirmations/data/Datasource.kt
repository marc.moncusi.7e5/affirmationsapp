/*
 * Copyright (C) 2021 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.affirmations.data

import com.example.affirmations.R
import com.example.affirmations.model.Affirmation
/**
 * [Datasource] generates a list of [Affirmation]
 */
class Datasource() {
    fun loadAffirmations(): List<Affirmation> {
        return listOf<Affirmation>(
            Affirmation(
                id = 1,
                stringResourceId = R.string.affirmation1,
                imageResourceId = R.drawable.image1,
                descriptionResourceId = R.string.description,
                emailTo = R.string.emailTo,
                emailSubject = R.string.emailSubject,
                phone = R.string.phoneNumber
            ),
            Affirmation(
                id = 2,
                stringResourceId = R.string.affirmation2,
                imageResourceId = R.drawable.image2,
                descriptionResourceId = R.string.description,
                emailTo = R.string.emailTo,
                emailSubject = R.string.emailSubject,
                phone = R.string.phoneNumber
            ),
            Affirmation(
                id = 3,
                stringResourceId = R.string.affirmation3,
                imageResourceId = R.drawable.image3,
                descriptionResourceId = R.string.description,
                emailTo = R.string.emailTo,
                emailSubject = R.string.emailSubject,
                phone = R.string.phoneNumber
            ),
            Affirmation(
                id = 4,
                stringResourceId = R.string.affirmation4,
                imageResourceId = R.drawable.image4,
                descriptionResourceId = R.string.description,
                emailTo = R.string.emailTo,
                emailSubject = R.string.emailSubject,
                phone = R.string.phoneNumber
            ),
            Affirmation(
                id = 5,
                stringResourceId = R.string.affirmation5,
                imageResourceId = R.drawable.image5,
                descriptionResourceId = R.string.description,
                emailTo = R.string.emailTo,
                emailSubject = R.string.emailSubject,
                phone = R.string.phoneNumber
            ),
            Affirmation(
                id = 6,
                stringResourceId = R.string.affirmation7,
                imageResourceId = R.drawable.image7,
                descriptionResourceId = R.string.description,
                emailTo = R.string.emailTo,
                emailSubject = R.string.emailSubject,
                phone = R.string.phoneNumber
            ),
            Affirmation(
                id = 8,
                stringResourceId = R.string.affirmation8,
                imageResourceId = R.drawable.image8,
                descriptionResourceId = R.string.description,
                emailTo = R.string.emailTo,
                emailSubject = R.string.emailSubject,
                phone = R.string.phoneNumber
            ),
            Affirmation(
                id = 8,
                stringResourceId = R.string.affirmation8,
                imageResourceId = R.drawable.image8,
                descriptionResourceId = R.string.description,
                emailTo = R.string.emailTo,
                emailSubject = R.string.emailSubject,
                phone = R.string.phoneNumber
            ),
            Affirmation(
                id = 9,
                stringResourceId = R.string.affirmation9,
                imageResourceId = R.drawable.image9,
                descriptionResourceId = R.string.description,
                emailTo = R.string.emailTo,
                emailSubject = R.string.emailSubject,
                phone = R.string.phoneNumber
            ),
            Affirmation(
                id = 10,
                stringResourceId = R.string.affirmation10,
                imageResourceId = R.drawable.image10,
                descriptionResourceId = R.string.description,
                emailTo = R.string.emailTo,
                emailSubject = R.string.emailSubject,
                phone = R.string.phoneNumber
            ),
        )
    }
}

package com.example.affirmations.viewmodel

import androidx.lifecycle.ViewModel
import com.example.affirmations.data.Datasource
import com.example.affirmations.model.Affirmation
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow

class MainViewModel : ViewModel() {
  private val _uiState = MutableStateFlow(Datasource().loadAffirmations())
  val uiState: StateFlow<List<Affirmation>> = _uiState.asStateFlow()
}

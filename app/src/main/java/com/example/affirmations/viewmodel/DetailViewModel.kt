package com.example.affirmations.viewmodel

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import androidx.lifecycle.ViewModel
import com.example.affirmations.R
import com.example.affirmations.data.Datasource
import com.example.affirmations.model.Affirmation
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow

class DetailViewModel(affirmationId: Int): ViewModel() {
  private val _uiState = MutableStateFlow(getAffirmationDataFromId(affirmationId))
  val uiState: StateFlow<Affirmation> = _uiState.asStateFlow()

  private fun getAffirmationDataFromId(affirmationId: Int): Affirmation {
    val affirmationList = Datasource().loadAffirmations()

    for (affirmation in affirmationList)
      if (affirmation.id == affirmationId)
        return affirmation

    return Affirmation(
      1,
      R.string.affirmation1,
      R.drawable.image1,
      R.string.description,
      R.string.emailTo,
      R.string.emailSubject,
      R.string.phoneNumber
    )
  }
}

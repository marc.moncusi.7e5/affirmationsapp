package com.example.affirmations

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Mail
import androidx.compose.material.icons.filled.Phone
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.modifier.modifierLocalConsumer
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.PlatformTextStyle
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.LineHeightStyle
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.em
import androidx.compose.ui.unit.sp
import com.example.affirmations.model.Affirmation
import com.example.affirmations.ui.theme.AffirmationsTheme
import com.example.affirmations.viewmodel.DetailViewModel


class DetailActivity : ComponentActivity() {
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContent {
      AffirmationDetailApp(intent.getIntExtra("affirmationId", 0))
    }
  }
}

@Composable
fun AffirmationDetailApp(Id: Int){
  AffirmationsTheme() {
    AffirmationDetail(DetailViewModel(Id))
  }
}
@Composable
fun AffirmationDetail(detailViewModel: DetailViewModel) {
  val uiState by detailViewModel.uiState.collectAsState()
  val context = LocalContext.current
  val emailTo: String = stringResource(id = uiState.emailTo)
  val emailSubject: String = stringResource(id = uiState.emailSubject)
  val phone: String = stringResource(id = uiState.phone)

  val imageModifier = Modifier
    .fillMaxWidth()
    .border(BorderStroke(3.dp, MaterialTheme.colors.secondary))
  Column(
  ) {
    Image(
      painter = painterResource(uiState.imageResourceId),
      contentDescription = "Stock Image",
      contentScale = ContentScale.Crop,
      modifier = Modifier
        .fillMaxWidth()
        .border(BorderStroke(3.dp, MaterialTheme.colors.secondary)),
    )
    Text(
      text = stringResource(id = uiState.stringResourceId),
      textAlign = TextAlign.Center,
      fontWeight = FontWeight.Bold,
      fontSize = 30.sp,
      modifier = Modifier
        .fillMaxWidth(),
    )
    Text(
      text = stringResource(id = uiState.descriptionResourceId),
      modifier = Modifier
        .fillMaxWidth()
        .padding(5.dp,0.dp),
    )
    Row(
      horizontalArrangement = Arrangement.End,
      modifier = Modifier
        .fillMaxWidth(),
    ) {
      IconButton(onClick = {context.sendMail(emailTo, emailSubject)}) {
        Icon(
          imageVector = Icons.Filled.Mail,
          contentDescription = "mail",
        )
      }
      IconButton(onClick = {context.dial(phone)} ) {
        Icon(
          imageVector = Icons.Filled.Phone,
          contentDescription = "phone"
        )
      }
    }
  }
}

private fun Context.sendMail(to: String, subject: String){
  try{
    val intent = Intent(Intent.ACTION_SEND)
    intent.type = "message/rfc822"
    intent.putExtra(Intent.EXTRA_EMAIL, arrayOf(to))
    intent.putExtra(Intent.EXTRA_SUBJECT, subject)
    startActivity(intent)
  }catch(e: ActivityNotFoundException){

  }catch(t: Throwable){

  }
}
private fun Context.dial(phone: String){
  try{
    val intent = Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null))
    startActivity(intent)
  }catch(t: Throwable){

  }
}


@Preview
@Composable
private fun AffirmationDetailPreview() {
  AffirmationDetail(DetailViewModel(1))
}